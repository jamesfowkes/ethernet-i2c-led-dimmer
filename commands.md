# Commands

## dimmer1/X, dimmer2/X, dimmer3/X, dimmer4/X

Individual dimming controls for each zone, where X is a percentage from 0 to 100.

For example, to set the value of dimmer3 to 75%, use the command /dimmer3/75.

## /all/X

Change the setting for all LEDs at once. For example to set all zones to 30%, use /all/30.

## /save

Save the current zone settings to memory. These settings will be restored when the Arduino is reset or when the /restore command is used.

## /restore

Restore the last saved zone settings.

## /flicker/X, /flicker/X/Y

Run the pre-programmed flicker pattern on outputs X.
If Y is provided, end the sequence with that brightness.

For example:

/flicker/1234 will flicker all zones
/flicker/12 will flicker zones 1 and 2. Other zones will be unaffected.
/flicker/1234/0 will flicker all zones and turn the zones off at the end (0% brightness)
/flicker/23/60 will flicker zones 2 and 3 and turn those zones to 60% brightness at the end. Other zones will be unaffected.

## /get/X

Get the current brightness of zone X
