#ifndef _FLICKER_H_
#define _FLICKER_H_

#define NO_CHANNEL (0xff)

typedef void (*on_flicker_complete_fn)(uint8_t const * const channels);
void setChannel(const uint8_t channel, const uint8_t value, bool bLog=true);

void flicker_setup(StringParam * pValues);
void flicker_start(uint8_t * channels, on_flicker_complete_fn on_flicker_complete);
void flicker_run(void);

#endif
